package com.iesportada.semipresencial.hlcto04narvaiza;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.iesportada.semipresencial.hlcto04narvaiza.databinding.ActivityMainBinding;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private static final int REQUEST_CONNECT = 1;
    private static double rate = 0;
    private double dolarAmount;
    private double euroAmount;
    private ActivityMainBinding binding;
    IntentFilter intentFilter;
    BroadcastReceiver broadcastReceiver;
    public static final String WEB = "http://rnarvaiza.me/files/dolarRatio.txt";

    public static final String ACTION_RESP = "RESPUESTA_DESCARGA";
    private Button botonIniciar, botonParar;
    private Switch switch1;
    private EditText editTextDolar, editTextEuro;
    private TextView salida;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View v = binding.getRoot();
        setContentView(v);

        botonIniciar = binding.botonIniciar;
        botonParar = binding.botonParar;
        switch1 = binding.switch1;
        editTextDolar = binding.editTextDolar;
        editTextEuro = binding.editTextEuro;
        salida = binding.salida;

        botonIniciar.setOnClickListener(this);
        botonParar.setOnClickListener(this);

        intentFilter = new IntentFilter(ACTION_RESP);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        broadcastReceiver = new ReceptorOperacion();
    }

    @Override
    public void onResume(){
        super.onResume();
        //---registrar el receptor ---
        registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void onPause(){
        super.onPause();
        //--- anular el registro del recpetor ---
        unregisterReceiver(broadcastReceiver);
    }

    public class ReceptorOperacion extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String respuesta = intent.getStringExtra("resultado");
            salida.setText(respuesta);

        }
    }


    @Override
    public void onClick(View v) {

        salida.setText("");

        if (v == botonIniciar) {
            if (comprobarPermiso()) {
                if (!switch1.isChecked()) {

                    // uso con Service
                    startService(new Intent(MainActivity.this, DownloadService.class));

                } else {
                    // uso con IntentService

                    Intent i = new Intent(this, DownloadIntentService.class);
                    i.putExtra("web", WEB);
                    startService(i);
                }
            }
        }

        if (v == botonParar) {
            if (!switch1.isChecked()) {
                stopService(new Intent(MainActivity.this, DownloadService.class));
            } else {
                stopService(new Intent(MainActivity.this, DownloadIntentService.class));
            }
            getRateFromSD();
            showToast("Valor del cambio recibido: " + String.valueOf(getRate()));
        }
    }

    private boolean comprobarPermiso() {
        //https://developer.android.com/training/permissions/requesting?hl=es-419
        String permiso = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        // Manifest.permission.INTERNET
        boolean concedido = false;
        // comprobar los permisos
        if (ActivityCompat.checkSelfPermission(this, permiso) != PackageManager.PERMISSION_GRANTED) {
            // pedir los permisos necesarios, porque no están concedidos
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permiso)) {
                concedido = false;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{permiso}, REQUEST_CONNECT);
                // Cuando se cierre el cuadro de diálogo se ejecutará onRequestPermissionsResult
            }
        } else {
            concedido = true;
        }
        return concedido;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        String permiso = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        //Manifest.permission.INTERNET;
        // chequeo los permisos de nuevo
        if (requestCode == REQUEST_CONNECT)
            if (ActivityCompat.checkSelfPermission(this, permiso) == PackageManager.PERMISSION_GRANTED)
                // permiso concedido
                startService(new Intent(MainActivity.this, DownloadService.class));
            else
                // no hay permiso
                showToast("No se ha concedido permiso para conectarse a Internet");
    }

    public void onDolarClick(View view){


        editTextDolar.setInputType(InputType.TYPE_CLASS_NUMBER |
                InputType.TYPE_NUMBER_FLAG_DECIMAL |
                InputType.TYPE_NUMBER_FLAG_SIGNED);

        editTextDolar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextDolar.getText().toString().equals("NaN") || editTextDolar.getText().toString().equals("")){
                    showToast("Please input a number");
                    editTextDolar.setText("0");
                }else {
                    if(Double.valueOf(editTextDolar.getText().toString()) != 0){
                        try{
                            if (editTextDolar.hasFocus()){
                                setDolarAmount(Double.valueOf(editTextDolar.getText().toString()));
                                editTextEuro.setText(fromDolarToEuro());
                            }
                        } catch (NumberFormatException nfe){
                            showToast(nfe.getMessage());
                        }
                    }
                    else{
                        showToast("Introduzca un valor distinto de 0");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    public void onEuroClick(View view){

        editTextEuro.setInputType(InputType.TYPE_CLASS_NUMBER |
                InputType.TYPE_NUMBER_FLAG_DECIMAL |
                InputType.TYPE_NUMBER_FLAG_SIGNED); //Tratamos el input para evitar cualquier caracter que no sea digito.

        editTextEuro.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextEuro.getText().toString().equalsIgnoreCase("NaN") || editTextEuro.getText().toString().isEmpty()){
                    showToast("Please input a number");
                    editTextEuro.setText("0");
                }else {
                    if(Double.valueOf(editTextEuro.getText().toString()) != 0){ //Evitamos el valor 0 para que no nos de un resultado infinito
                        try{
                            if (editTextEuro.hasFocus()){
                                setEuroAmount(Double.valueOf(editTextEuro.getText().toString()));
                                editTextDolar.setText(fromEuroToDolar());
                            }
                        }catch (NumberFormatException nfe){//Sacamos excepción por si en algún momento se setease el editText algún mensaje con caracteres diferentes de los numéricos.
                            showToast(nfe.getMessage());
                        }
                    }else
                    {
                        showToast("Introduzca un valor distinto de 0");
                    }
                }

            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void getRateFromSD(){

        double rate = 0;
        String getString = "";
        String s = "";
        Scanner sc = null;
        File miFichero, tarjeta;
        try {
            tarjeta = Environment.getExternalStorageDirectory();
            miFichero = new File(tarjeta.getAbsolutePath(), "dolarRatio.txt");
            if (!miFichero.exists()) {
                showToast("No hay fichero de cambio descargado.");
                setRate(0.88);
            }
            sc = new Scanner(miFichero);
            getString = sc.next();
            s = getString.replace(",", ".");
            rate = Double.parseDouble(s);
            setRate(rate);
        }catch (FileNotFoundException e) {
            showToast(e.getMessage());
        }finally {
            if (sc != null){
                sc.close();
            }
        }

    }

    public String fromDolarToEuro(){
        String result = Double.toString((getRate())*(Double.valueOf(getDolarAmount())));

        return result;
    }

    public String fromEuroToDolar(){
        String result = Double.toString(((Double.valueOf(getEuroAmount()))/getRate()));
        return result;
    }

    private void showToast(String s){
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }


    public static double getRate() {
        return rate;
    }

    public static void setRate(double rate) {
        MainActivity.rate = rate;
    }

    public double getDolarAmount() {
        return dolarAmount;
    }

    public void setDolarAmount(double dolarAmount) {
        this.dolarAmount = dolarAmount;
    }

    public double getEuroAmount() {
        return euroAmount;
    }

    public void setEuroAmount(double euroAmount) {
        this.euroAmount = euroAmount;
    }
}